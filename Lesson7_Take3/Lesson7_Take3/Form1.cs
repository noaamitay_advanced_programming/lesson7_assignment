﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lesson7_Take3
{
    public partial class Form1 : Form
    {
        List<Image> images; //To save all the images
        NetworkStream clientStream;
        Thread myThread;
        PictureBox[] redCards = new PictureBox[10];
        int playerNumTurns;
        int opponentNumTurns;
        int playerPoints;
        int opponentPoints;
        int playerCardNum;
        int opponentCardNum;

        public Form1()
        {
            InitializeComponent();

            //Insert all the images from the resources to the list (not including the upside down cards)
            images = new List<Image>();
            DirectoryInfo di = new DirectoryInfo(@"PNG-cards");
            FileInfo[] finfos = di.GetFiles("*.png", SearchOption.AllDirectories);
            foreach (FileInfo fi in finfos)
                images.Add(Image.FromFile(fi.FullName));
        }

        /*
         * Input: none
         * Output: none
         * The function starts the game, it connects the client to the server and takes care of all the messages
        */
        private void GameOn()
        {
            string input = "";

            //connect to the server
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client.Connect(serverEndPoint);
            clientStream = client.GetStream();

            //initiate all the fields
            playerPoints = 0;
            opponentPoints = 0;
            playerNumTurns = 0;
            opponentNumTurns = 0;

            while (!input.Equals("2000")) //As long as the server didn't sent a message to end the connection
            {
                //Get a message from the server
                byte[] bufferIn = new byte[4];
                int bytesRead = clientStream.Read(bufferIn, 0, 4);
                input = new ASCIIEncoding().GetString(bufferIn);

                if (input[0] == '0') //If the message is a message to start a game (the two clients connected successfully)
                {
                    Invoke((MethodInvoker)delegate { btn.Enabled = true; GenerateCards(); });
                }
                else if (input[0] == '1') //If the sever sent a card code                  
                {
                    opponentNumTurns++; //The opponent played his turn

                    while (playerNumTurns < opponentNumTurns) { } //As long as the player didn't played his part yed don't calculate points and don't continue
                    
                    if ((playerNumTurns == opponentNumTurns) && playerNumTurns != 0) //If both players already chose a card each
                    {
                        Invoke((MethodInvoker)delegate { CheckBigger(); }); //Check who has the bigger card
                    }

                    OpponentsCard(input); //Show the card of the opponent
                }
            }

            MessageBox.Show("Your Score: " + playerPoints + "\nOpponent's Score: " + opponentPoints); //Show the score
        }

        /*
         * Input: none
         * Output: none         
         * The function checks who won this round of the game and changes the number points of each player
        */ 
        private void CheckBigger()
        {
            if (playerCardNum > opponentCardNum) //If the player won this round
            {
                playerPoints++;
                opponentPoints--;
            }
            else if (playerCardNum < opponentCardNum) //If the opponent won this round
            {
                opponentPoints++;
                playerPoints--;
            }

            //Change the labels of each player's points to the updated points
            label4.Text = playerPoints.ToString();
            label2.Text = opponentPoints.ToString();
        }

        /*
         * Input: the message from the client (opponent's card code) 
        */ 
        private void OpponentsCard(string input)
        {
            string numCardStr = "";
            int numCard = 0, kindCard = 0;

            if (input[1] == '0') //If the card number has one digit
            {
                numCardStr = input[2].ToString();
                numCard = Int32.Parse(numCardStr);
            }
            else //If the card number has two digits
            {
                numCardStr = input[1].ToString() + input[2].ToString();
                numCard = Int32.Parse(numCardStr);
            }

            //If the card kind is...
            if (input[3] == 'C') //Clubs
            {
                kindCard = 1;
            }
            else if (input[3] == 'D') //Diamonds
            {
                kindCard = 2;            
            }
            else if (input[3] == 'H') //Hearts
            {
                kindCard = 3;
            }
            else if (input[3] == 'S') //Speads
            {
                kindCard = 4;
            }

            int place = ((numCard * 4) - 4) + (kindCard - 1); //Calculate the place of the image in the list of images (using the card number and kind)
            pictureBox1.Image = images[place]; //Change the image of the opponent's card into the image in the right place in the list

            opponentCardNum = numCard;
        }

        /*
         * Input: none
         * Output: none
         * The function creates all the player's cards and creates an event to them
        */ 
        private void GenerateCards()
        {
            // set the location for where we start drawing the new cards (notice the location+height)
            Point nextLocation = new Point(15, this.Size.Height - 100 - this.pictureBox1.Height);

            for (int i = 0; i < 10; i++)
            {
                // create the image object itself
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Image = global::Lesson7_Take3.Properties.Resources.card_back_red;
                currentPic.Name = "pictureBox" + i+2;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(pictureBox1.Width, pictureBox1.Height);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                redCards[i] = currentPic;

                // assign an event to it
                currentPic.Click += delegate (object sender1, EventArgs e1)
                {
                    //Initiate all the cards to their back image
                    pictureBox1.Image = global::Lesson7_Take3.Properties.Resources.card_back_blue;                
                    for (int j = 0; j < redCards.Length; j++)
                    {
                        redCards[j].Image = global::Lesson7_Take3.Properties.Resources.card_back_red;
                    }

                    playerNumTurns++;

                    //Choose a random image
                    Random random = new Random();
                    int place = random.Next(0, images.Count - 1);
                    currentPic.Image = images[place];

                    //Calculate the card's number and kind
                    int numCard = (place / 4) + 1;
                    int kindCard = (place % 4) + 1;
                    string kind = "";
                    string num = numCard.ToString();

                    if (numCard < 10) //If the card's number has only 1 digit
                    {
                        num = "0" + numCard.ToString();
                    }

                    switch (kindCard) //Check the card's kind
                    {
                        case 1:
                            kind = "C";
                            break;
                        case 2:
                            kind = "D";
                            break;
                        case 3:
                            kind = "H";
                            break;
                        case 4:
                            kind = "S";
                            break;
                    }                
                    
                    string message = "1" + num + kind; //Creates a card code message
                    playerCardNum = numCard;

                    //Send the message
                    byte[] buffer = new ASCIIEncoding().GetBytes(message);
                    clientStream.Write(buffer, 0, 4);
                    clientStream.Flush();                   
                };

                // add the picture object to the form (otherwise it won't be seen)
                this.Controls.Add(currentPic);

                // calculate the location point for the next card to be drawn/added
                nextLocation.X += currentPic.Size.Width + 10;
            }
        }

        /*
         * The function starts when the form is immidiatly loaded
         * It diables the quit button and starts a thread
        */ 
        private void Form1_Load(object sender, EventArgs e)
        {
            btn.Enabled = false;
            myThread = new Thread(GameOn);
            myThread.Start();
        }

        private void label1_Click(object sender, EventArgs e) { }

        /*
         * The function starts when the user clicks on the quit button 
         * It sends a message to the server to finish the game and quit
        */ 
        private void button1_Click(object sender, EventArgs e)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes("2000");
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();
        }

        /*
         * The function starts when the form is closed
         * It sends a message to the server to finish the game
        */ 
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes("2000");
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();
            myThread.Abort(); //Finish the thread
        }
    }
}
